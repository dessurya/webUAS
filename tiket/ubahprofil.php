<?php

include_once ("include/class.php");
$member = new member;
$member->setUsername($_SESSION['usernameMember']);
$member->cekUserByUsername();

	//Jika tombol submit ditekan (Terjadi request)
	if($_SERVER['REQUEST_METHOD'] == "POST"){
		//Jika tidak dipilih salah 1 opsi penggantian identitas namun tombolnya diklik, maka:
		if(!(isset($_POST['gantinama'])) and !(isset($_POST['gantiusername'])) and !(isset($_POST['gantipassword'])) and !(isset($_POST['gantiemail']))){			
			$nama = $member->getNama();
			$username = $member->getUsername();
			$email = $member->getEmail();
			?>
            	<script>
					alert('Setidaknya pilih salah satu field yang ingin dirubah');
				</script>
                
            <?
		}
		else{
			$oldPassword = $_POST['oldPassword'];
			$member->setPassword($oldPassword);
			
			//Lakukan pengecekan input password lama dan username lama, apakah sesuai?
			$message = '';
			$mainErrorMsg = 'Terjadi Kesalahan: ';
			
			if($member->cekCocokUsernameDanPassword() == 0){		
				$message = $mainErrorMsg.'\nPassword yang anda masukkan tidak sesuai.';				
			}
			else{
				if(isset($_POST['gantinama'])){
					$member->setNamaField('nama');
					$member->setNilai($_POST['nama']);
					
					if($member->updateMember()==1){
						$message = '\nNama berhasil diperbaharui.';
					}
					else{
					}
				}
				if(isset($_POST['gantiemail'])){
					$member->setEmail($_POST['email']);
					
					//mengecek, apakah email sudah digunakan
					if($member->cekEmail()==FALSE){//Email Belum Digunakan
						$member->setNamaField('email');
						$member->setNilai($_POST['email']);
						
						if($member->updateMember()==1){
							$message = $message.'\nEmail Berhasil Diperbaharui';
						}
						else{
							$message = $message.'\nEmail Gagal Diperbaharui';
						}
					}
					else{
						$message = $message.'\nEmail yang Diinput Sudah Digunakan, Gagal Diperbaharui.';
					}
				}
				if(isset($_POST['gantipassword'])){
					$password = $_POST['password'];
					$rePassword = $_POST['Repassword'];
					
					if($password != $rePassword){//Apakah 2 Password yang dinputkan sama?
						$message = $message.'\nPassword yang Diinput Tidak Sama, Password Gagal Diperbaharui.';
					}
					else{
						$member->setNamaField('password');
						$member->setNilai($password);
						
						if($member->updateMember()==1){
							$message = $message.'\nPassword Berhasil Diperbaharui.';
						}
						else{
							$message = $message.'\nPassword Gagal Diperbaharui.';
						}
					}
				}
				if(isset($_POST['gantiusername'])){
					$newUsername = $_POST['username'];
					$member->setUsername($newUsername);
					
					if($member->cekUserByUsername() == 1){//Bagaimana apabila username sudah digunakan?
						$message = $message.'\nUsername yang Diinput Sudah Digunakan, Username Gagal Diperbaharui.';
					}
					else{
						$member->setNamaField('username');
						$member->setUsername($_SESSION['usernameMember']);
						$member->setNilai($newUsername);
						
						if($member->updateMember()==1){
							$message = $message.'\nBerhasil Memperbaharui Username.';
							$_SESSION['usernameMember'] = $member->getNilai();
						}
						else{
							$message = $message.'\nGagal Memperbaharui Username';
						}
					}
				}
				
			}	
			?>
            <script language="javascript">
				alert('<?php echo $message;?>');
				window.location='?page=ubahprofil';
			</script>
            <?php
		}
	}
	else{
	$nama = $member->getNama();
	$username = $member->getUsername();
	$email = $member->getEmail();
	}
	

?>
    <h2 class="text-center">
    Profil Anda
    </h2>
    <div class="container">
    <hr>
    <form method="post">
        <table style="font-size:14px" align="center" width="75%">
            <tr>
                <td colspan="4">
                    Centang tiap opsi berikut ini apabila anda ingin menggantinya, kemudian isi sesuai petunjuk pada masing-masing field tersebut:
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <hr>
                </td>
            </tr>
            <tr>
                <td>
                    <input type="checkbox" name="gantinama" value="1">
                </td>
                <td>
                    <h4>Nama Lengkap</h4>
                </td>
                <td>
                    <h4>:</h4>
                </td>
                <td>
                    <input type="text" name="nama" id="nama" maxlength="15" class="form-control" style="width:300px;" placeholder="1-15 Karakter" pattern="[a-zA-Z\s]{1,30}" value="<?php if(isset($nama)){echo $nama;}?>">
                </td>
                <td>
                    <span id="nama">
                    </span>
                </td>
            </tr>
            <tr>
                <td>
                    <input type="checkbox" name="gantiusername" value="1">
                </td>
                <td width="28%">
                    <h4>Usename</h4>
                </td>
                <td width="5%">
                    <h4>:</h4>
                </td>
                <td width="57%">
                    <input type="text" name="username" id="username" maxlength="15" class="form-control" style="width:300px;" placeholder="5-15 Karakter" value="<?php if(isset($username)){echo $username;} ?>">
                </td>
                <td width="10%">
                    <span id="userCapt">
                    </span>
                </td>	
            </tr>
            <tr>
                <td>
                    <input type="checkbox" name="gantipassword" value="1">
                </td>
                <td>
                    <h4>Password Baru</h4>
                </td>
                <td>
                    <h4>:</h4>
                </td>
                <td>
                    <input type="password" name="password" id="password" maxlength="15" class="form-control" style="width:300px;" placeholder="4-15 Karakter (kapital, non kapital & angka)" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{4,}">
                </td>
                <td>
                    <span id="passCapt">
                    </span>
                </td>
            </tr>
            <tr>
                <td>&nbsp;
                    
                </td>
                <td>
                    <h4>Ulangi Password Baru</h4>
                </td>
                <td>
                    <h4>:</h4>
                </td>
                <td>
                    <input type="password" name="Repassword" id="Repassword" maxlength="15" class="form-control" style="width:300px;" placeholder="4-15 Karakter (kapital, non kapital & angka)" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{4,}">
                </td>
                <td>
                    <span id="passCapt">
                    </span>
                </td>
            </tr>
            <tr>
                <td>
                    <input type="checkbox" name="gantiemail" value="1">
                </td>
                <td>
                    <h4>Email</h4>
                </td>
                <td>
                    <h4>:</h4>
                </td>
                <td>
                    <input type="email" name="email" id="email" class="form-control" style="width:300px;" placeholder="" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" value="<?php if(isset($email)){echo $email;}?>">
                </td>
                <td>
                    <span id="emailCapt">
                    </span>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <hr>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    Silahkan isi password lama anda untuk melakukan perubahan profil
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <h4>Password Lama</h4>
                </td>
                <td>
                    <h4>:</h4>
                </td>
                <td>
                    <input type="password" name="oldPassword" id="password" maxlength="15" class="form-control" style="width:300px;" placeholder="4-15 Karakter (kapital, non kapital & angka)" required pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{4,}">
                </td>
                <td>
                    <span id="passCapt">
                    </span>
                </td>
            </tr>
            <tr>
                <td colspan="4" align="center">
                <br>
                    <input type="submit" name="Daftar" class="btn btn-default" value="Ubah Profil">
                </td>
            </tr>
    </table>
    </form>
    </div>
