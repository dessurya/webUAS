<?php
	$page = isset($_GET['page']) ? $_GET['page'] : null;
	
	switch ($page)
		{
			case '': if (!file_exists ("beranda.php")) die ("<center><h1>404: Tidak Ditemukan</h1></center>");
			include "beranda.php";
			break;
			
			case 'memberlogin': if (!file_exists ("memberlogin.php")) die ("<center><h1>404: Tidak Ditemukan</h1></center>");
			include "memberlogin.php";
			break;
			
			case 'ubahprofil': if (!file_exists ("ubahprofil.php")) die ("<center><h1>404: Tidak Ditemukan</h1></center>");
			include "ubahprofil.php";
			break;
			
			case 'lihatprofil': if (!file_exists ("lihatprofil.php")) die ("<center><h1>404: Tidak Ditemukan</h1></center>");
			include "lihatprofil.php";
			break;
			
			case 'daftarmember': if (!file_exists ("daftarmember.php")) die ("<center><h1>404: Tidak Ditemukan</h1></center>");
			include "daftarmember.php";
			break;
			
			case 'rincianTiket': if(!file_exists ("rincianTiket.php")) die ("<center><h1>404: Tidak Ditemukan</h1></center>");
			include "rincianTiket.php";
			break;
			
			case 'detailorder': if(!file_exists ("detailorder.php")) die ("<center><h1>404: Tidak Ditemukan</h1></center>");
			include "detailorder.php";
			break;
			
			case 'updateJmlTiket': if(!file_exists ("updateJmlTiket.php")) die ("<center><h1>404: Tidak Ditemukan</h1></center>");
			include "updateJmlTiket.php";
			break;
			
			case 'uploadbuktitransfer': if(!file_exists ("uploadbuktitransfer.php")) die ("<center><h1>404: Tidak Ditemukan</h1></center>");
			include "uploadbuktitransfer.php";
			break;
			
			case 'batalorder': if(!file_exists ("batalorder.php")) die ("<center><h1>404: Tidak Ditemukan</h1></center>");
			include "batalorder.php";
			break;
			
			case 'orderan' or 'tambahtiket': if(!file_exists ("orderan.php")) die ("<center><h1>404: Tidak Ditemukan</h1></center>");
			include "orderan.php";
			break;
			case 'print' : if(!file_exists ("printTiket.php")) die ("<center><h1>404: Tidak Ditemukan</h1></center>");
			include "printTiket.php";
			break;		
			
			default: include "beranda.php";
		}
?>