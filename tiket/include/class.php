<?php
	/**** Class Library ****/
	class database{
		private $host = "localhost";
		private $user = "root";
		private $password  = "";
		private $dbname = "db_tiketkonser";
		
		function connectmysql(){
			mysql_connect($this->host,$this->user,$this->password);
			mysql_select_db($this->dbname) or die("Database Tidak Ditemukan");
		}
	}
	class member{
		
		//Deklarasi Variabel
		private $username, $noTelp, $password, $nama, $email, $sesi, $namaField, $nilai;
		
		/*================== Methode Set pada Member ======================*/
		
		public function setUsername($username){
			$this->username = $username;
		}
		
		public function setNamaField($namaField){
			$this->namaField = $namaField;
		}
		
		public function setNilai($nilai){
			$this->nilai = $nilai;
		}
		
		public function setEmail($email){
			$this->email = $email;
		}
		
		public function setNoTelp($noTelp){
			$this->noTelp = $noTelp;
		}
		
		public function setPassword($password){
			$this->password = $password;
		}
		
		public function setNama($nama){
			$this->nama = $nama;
		}
		
		public function setLogin($username, $password){
			$_SESSION['loginMember'] = TRUE;
			$_SESSION['usernameMember'] = $username;
			$_SESSION['passwordMember'] = $password;
		}
		
		
		/*================== Methode Get pada Member ======================*/
		
		public function getUsername(){
			return $this->username;
		}
		
		public function getEmail(){
			return $this->email;
		}
		
		public function getNoTelp(){
			return $this->noTelp;
		}
		
		public function getPassword(){
			return $this->password;
		}
		
		public function getNama(){
			return $this->nama;
		}
		
		public function getNamaField(){
			return $this->namaField;
		}
		
		public function getNilai(){
			return $this->nilai;
		}
		
		
		
		/*==================== Methode dengan melakukan cek pada database ======================*/
		
		function cekUsername(){
			$sql1 = "SELECT *FROM pelanggan WHERE username='".$this->username."'";
			$query1 = mysql_query($sql1) or die("Error 1: ".mysql_error());
			$row1 = mysql_fetch_array($query1);
			
			if($row1 != 0){
				return $row1;
			}
			else{
				return FALSE;
			}
		}
		function cekUserByUsername(){
			$sql1 = "SELECT *FROM pelanggan WHERE username='".$this->getUsername()."'";
			$query1 = mysql_query($sql1) or die("Error 1: ".mysql_error());
			$row1 = mysql_fetch_array($query1);
			
			if($row1 != 0){				
				$this->setEmail($row1['email']);
				$this->setNama($row1['nama']);
				$this->setNoTelp($row1['noTelp']);
				$this->setPassword($row1['password']);
				
				//return harus selalu dibawah dari semuanya...
				return 1;
			}
			else{
				return 0;
			}
		}
		public function cekPassword(){
			$sql2 = "SELECT *FROM pelanggan WHERE password='".$this->password."'";
			$query2 = mysql_query($sql2) or die("Error2: ".mysql_error());
			$row2 = mysql_num_rows($query2);
			
			if(row2 == 0){
				return FALSE;
			}
			else{
				return TRUE;
			}
		}
		public function cekEmail(){
			$sql1 = "SELECT *FROM pelanggan WHERE email='".$this->email."'";
			$query1 = mysql_query($sql1) or die("Error 4: ".mysql_error());
			$row1 = mysql_fetch_array($query1);
			
			if($row1 != 0){
				return $row1;
			}
			else{
				return FALSE;
			}
		}
		public function cekTelpon(){
			$sql1 = "SELECT *FROM pelanggan WHERE noTelp='".$this->noTelp."'";
			$query1 = mysql_query($sql1) or die("Error 5: ".mysql_error());
			$row1 = mysql_fetch_array($query1);
			
			if($row1 != 0){
				return $row1;
			}
			else{
				return FALSE;
			}
		}
		public function cekUsernameDanPassword(){
			$sql3 = "SELECT *FROM pelanggan WHERE username='".$this->username."' AND password='".$this->password."'";
			$query3 = mysql_query($sql3) or die("Error 3: ".mysql_error());
			$row3 = mysql_fetch_array($query3);
			
			if($row3 != 0){
				return $row3;
			}
			else{
				return FALSE;
			}
		}
		public function cekCocokUsernameDanPassword(){
			$sql3 = "SELECT *FROM pelanggan WHERE username='".$this->getUsername()."' AND password='".$this->getPassword()."'";
			$query3 = mysql_query($sql3) or die("Error 3: ".mysql_error());
			$row3 = mysql_fetch_array($query3);
			
			if(mysql_num_rows($query3)!=0){
				return 1;
			}
			else{
				return 0;
			}
		}
		function updateMember(){
			$query = "UPDATE pelanggan SET ".$this->getNamaField()."='".$this->getNilai()."' WHERE username='".$this->getUsername()."'";
			$sql = mysql_query($query) or die ("Error Bro: ".mysql_error());
			
			if($sql){
				
				return 1;
			}
			else{
				return 0;
			}
		}
		
		/*==================== Methode untuk melakukan Insert Data ke Database ====================*/
		public function memberBaru(){
			$sql = "INSERT INTO pelanggan(username,email,noTelp,password,nama) VALUES ('".$this->getUsername()."','".$this->getEmail()."','".$this->getNoTelp()."','".$this->getPassword()."','".$this->getNama()."')";
			
			$query = mysql_query($sql) or die(mysql_error());
			
			if($query){
				return TRUE;
			}
			else{
				return FALSE;
			}
		}
		
		/*======================================================================================*/
		function logout(){
			$_SESSION['loginMember'] = FALSE;
			session_destroy();
			return 1;
		}

	}
	class tiket{
		private $keyCari, $awalRecord, $banyakRecord, $queryDB, $idTiket, $gambar, $nmEven, $rincian, $awalEven, $akhirEven, $harga, $stok, $idKategori, $jumlahAwal, $jumlahAkhir;
		
	/*====================== Methode Set Cari ===================*/
		public function setCari($keyCari){
			$this->keyCari = $keyCari;
		}
		public function setAwalRecord($awal){
			$this->awalRecord = $awal;
		}
		public function setBanyakRecord($banyakRecord){
			$this->banyakRecord = $banyakRecord;
		}
		public function setQueryDB($queryDB){
			$this->queryDB = $queryDB;
		}
		public function setIdTiket($idTiket){
			$this->idTiket = $idTiket;
		}
		public function setGambar($gambar){
			$this->gambar = $gambar;
		}
		public function setNmEven($nmEven){
			$this->nmEven = $nmEven;
		}
		public function setRincian($rincian){
			$this->rincian = $rincian;
		}
		public function setAwalEven($awalEven){
			$this->awalEven = $awalEven;
		}
		public function setAkhirEven($akhirEven){
			$this->akhirEven = $akhirEven;
		}
		public function setHarga($harga){
			$this->harga = $harga;
		}
		public function setIdKategori($idKategori){
			$this->idKategori = $idKategori;
		}
		public function setStok($stok){
			$this->stok = $stok;
		}
		public function setJumlahAwal($jumlahAwal){
			$this->jumlahAwal = $jumlahAwal;
		}
		public function setJumlahAkhir($jumlahAkhir){
			$this->jumlahAkhir = $jumlahAkhir;
		}
	
	/*====================== Methode Get Cari ===================*/
		public function getCari(){
			return $this->keyCari;
		}
		public function getAwalRecord(){
			return ($this->awalRecord);
		}
		public function getBanyakRecord(){
			return $this->banyakRecord;
		}
		public function getIdTiket(){
			return $this->idTiket;
		}
		public function getGambar(){
			return $this->gambar;
		}
		public function getNmEven(){
			return $this->nmEven;
		}
		public function getRincian(){
			return $this->rincian;
		}
		public function getAwalEven(){
			return $this->awalEven;
		}
		public function getAkhirEven(){
			return $this->akhirEven;
		}
		public function getHarga(){
			return $this->harga;
		}
		public function getIdKategori(){
			return $this->idKategori;
		}
		public function getStok(){
			return $this->stok;
		}
		public function getJumlahAwal(){
			return $this->jumlahAwal;
		}
		public function getJumlahAkhir(){
			return $this->jumlahAkhir;
		}
	/*====================== Methode yang Mengakses Database =========================*/
		
		function univCariTiketLimit() //fungsi ini digunakan untuk mencari tiket sesuai kata kunci yang dimasukkan secara universal dengan batas tertentu
		{
			$sql = "SELECT *FROM tiket WHERE nmEven LIKE '%".$this->getCari()."%' ORDER BY awalEven LIMIT ".$this->getAwalRecord().",".$this->getBanyakRecord()."";
			$query = mysql_query($sql) or die("Error: ".mysql_error());
			
			
			if(mysql_num_rows($query)==0){
				return FALSE;
			}
			else{
				return $query;
			}
		}
		
		function univCariTiket() //fungsi ini digunakan untuk mencari tiket sesuai kata kunci yang dimasukkan secara universal tanpa
		{
			$sql = "SELECT *FROM tiket WHERE nmEven LIKE '%".$this->getCari()."%' ORDER BY awalEven";
			$query = mysql_query($sql) or die("Error: ".mysql_error());
			
			
			if(mysql_num_rows($query)==0){
				return FALSE;
			}
			else{
				return $query;
			}
		}
		
		function tiketTerbaruLimit() //fungsi untuk mengambil data tiket dari terbaru dengan limit 6 tiket per halaman
		{
			$sql = "SELECT *FROM tiket ORDER BY awalEven LIMIT ".$this->getAwalRecord().",".$this->getBanyakRecord()."";
			$query = mysql_query($sql) or die("Error: ".mysql_error());
			
			if(mysql_num_rows($query)==0){
				return FALSE;
			}
			else{
				return $query;
			}
		}
		
		function tiketTerbaru() //fungsi untuk mengambil data tiket terbaru tanpa limit
		{
			$sql = "SELECT *FROM tiket ORDER BY awalEven";
			$query = mysql_query($sql) or die("Error: ".mysql_error());
			
			if(mysql_num_rows($query)==0){
				return FALSE;
			}
			else{
				return $query;
			}
		}
		
		function cariRincianTiket()
		{
			$sql = "SELECT *FROM tiket WHERE idTiket='".$this->getIdTiket()."'";
			$query = mysql_query($sql) or die("Error: ".mysql_error());
			
			if(mysql_num_rows($query)==0){
				return '0';
			}
			else{
				$row = mysql_fetch_array($query);
				$this->setGambar($row['gambar']);
				$this->setNmEven($row['nmEven']);
				$this->setRincian($row['rincian']);
				$this->setAwalEven($row['awalEven']);
				$this->setAkhirEven($row['akhirEven']);
				$this->setHarga($row['harga']);
				$this->setStok($row['stok']);
				$this->setIdKategori($row['idKategori']);
				
			}
		}
		
		function updateStokById() // melakukan update stok sesuai idTiket Terpilih sesuai dengan parameter yang ditentukan
		{
			$sql = "UPDATE tiket SET stok = '".$this->getJumlahAkhir()."' WHERE idTiket = '".$this->getIdTiket()."'";
			$query = mysql_query($sql) or die("Error: ".mysql_error());
			
			if($query){
				return 1;
			}
			else{
				return 0;
			}
		}
		
		
		
		
		
		
			
	/*===================== Method untuk Tampilan Tertentu ==========================*/
		function tampilTiket($nmEven, $rincian, $gambar, $harga, $idEven, $stok){
			if($stok<=0){
				$status = 'disabled';
				$string = 'Tiket Habis';
			}
			else{
				$status = '';
				$string = 'Order';
			}
			
			return '<div class="col-sm-4 col-md-4 col-lg-4 col-xs-6">
      <div class="thumbnail"> <img src="include/uploads/'.$gambar.'" alt="Thumbnail Image 1" class="img-responsive">
        <div class="caption">
          <h3>'.substr($nmEven,0,29).'</h3>
          <p>'.substr($rincian,0,30).' (<a href="?page=rincianTiket&evenID='.$idEven.'">selengkapnya..</a>)</p>
		  <p>
		  	IDR '.number_format($harga).'
		  </p>
          <p><a href="?page=tambahtiket&evenID='.$idEven.'" class="btn btn-primary '.$status.'" role="button"><span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> '.$string.'</a></p>
        </div>
      </div>
    </div>';
		}
	
	}
	class dtOrder{ 
		private $idOrder, $idTiket, $status, $jmlTiket, $idDiskon, $username, $idDetail;
		
		public function setIdTiket($idTiket){
			$this->idTiket = $idTiket;
		}
		public function setIdDetail($idDetail){
			$this->idDetail = $idDetail;
		}
		public function getIdDetail(){
			return $this->idDetail;
		}
		public function getIdTiket(){
			return $this->idTiket;
		}
		public function setStatus($status){
			$this->status = $status;
		}
		public function getStatus(){
			return $this->status;
		}
		public function setIdOrder($idOrder){
			$this->idOrder = $idOrder;
		}
		public function getIdOrder(){
			return $this->idOrder;
		}
		public function setJmlTiket($jmlTiket){
			$this->jmlTiket = $jmlTiket;
		}
		public function getJmlTiket(){
			return $this->jmlTiket;
		}
		public function setIdDiskon($idDiskon){
			$this->idDiskon = $idDiskon;
		}
		public function getIdDiskon(){
			return $this->idDiskon;
		}
		public function setUsername($username){
			$this->username = $username;
		}
		public function getUsername(){
			return $this->username;
		}
		/*=============================================================*/
		
		function cekDtOrderByIdTiket(){
			$sql = "SELECT *FROM dt_order WHERE idOrder='".$this->getIdOrder()."' AND idTiket='".$this->getIdTiket()."'";
			$query = mysql_query($sql) or die("Error Detail Order: ".mysql_error());
			
			if(mysql_fetch_array($query) == 0) //Jika tidak ada tiket pada detail order dengan id dan status yang diset
			{
				return 0;
			}
			else
			{
				$row = mysql_fetch_array($query);
				$this->setIdOrder($row['idOrder']);
				$this->setJmlTiket($row['jmlTiket']);
				return 1;
			}	
			
		}	
		function cekDtOrderByIdDetail(){
			$sql = "SELECT *FROM dt_order WHERE idDetail='".$this->getIdDetail()."'";
			$query = mysql_query($sql) or die("Error Detail Order: ".mysql_error());
			
			if(mysql_num_rows($query) == 0) //Jika tidak ada tiket pada detail order dengan id dan status yang diset
			{
				return 0;
			}
			else
			{
				$row = mysql_fetch_array($query);
				$this->setJmlTiket($row['jmlTiket']);
				$this->setIdTiket($row['idTiket']);
				$this->setIdDiskon($row['idDiskon']);
				$this->setIdOrder($row['idOrder']);
				
				return 1;
			}	
			
		}	
		function cekDtOrderByIdOrder(){
			$sql = "SELECT *FROM dt_order INNER JOIN tiket ON tiket.idTiket=dt_order.idTiket WHERE idOrder='".$this->getIdOrder()."'";
			$query = mysql_query($sql) or die("Error Detail Order: ".mysql_error());
			
			if(mysql_fetch_array($query) == 0) //Jika tidak ada tiket pada detail order dengan id dan status yang diset
			{
				return 0;
			}
			else
			{	
				return $sql;
			}
		}
		function tambahDtOrder(){
			$sql = "INSERT INTO dt_order(idOrder, idTiket, jmlTiket, idDiskon) VALUES ('".$this->getIdOrder()."','".$this->getIdTiket()."','".$this->getJmlTiket()."','".$this->getIdDiskon()."')";
			$query = mysql_query($sql) or die("Error Tambah Detail: ".mysql_error());
			
			if($query){
				return 1;
			}
			else{
				return 0;
			}
		}
		function updateJmlOrderan(){
			$sql = "UPDATE dt_order
			SET jmlTiket = jmlTiket+".$this->getJmlTiket()." 
			WHERE 
			idOrder='".$this->getIdOrder()."' 
			AND idTiket='".$this->getIdTiket()."'";
			$query = mysql_query($sql) or die("Error Menambahkan Jumlah Tiket: ".mysql_error());
			
			if($query){
				return 1;
			}
			else{
				return 0;
			}
		}
		function hapusDtOrder(){
			$sql = "DELETE FROM dt_order WHERE idDetail='".$this->getIdDetail()."'";
			$query = mysql_query($sql) or die("Error Tambah Detail: ".mysql_error());
			
			if($query){
				return 1;
			}
			else{
				return 0;
			}
		}
		
	}
	class COrder{
		private $idOrder, $tglOrder, $subtotal, $buktiTrf, $status, $idAdmin, $username;
		
		public function setIdOrder(){
			$waktu = new waktu;
			$waktu->setWaktuSekarang();
			$tglSekarang = substr($waktu->getWaktuSekarang(),0,10);
			
			$sql = "SELECT idOrder FROM tb_order WHERE tglOrder LIKE '$tglSekarang%' ORDER BY idOrder DESC";
			$query = mysql_query($sql) or die("Salah Nomor Order: ".mysql_error());
			
			//Melakukan pengecekan apakah pada tanggal aktual sudah ada transaksi atau tidak
			if(mysql_num_rows($query) == 0)
			{
				$idOrderBaru = str_replace("-","",$tglSekarang).'0001';
			}
			else{
				$row = mysql_fetch_array($query);
				$idOrderLama = $row['idOrder'];
				$ekorIdLama = substr($idOrderLama, 8);
				
				//Membuat idOrder Baru
				if($ekorIdLama>=1 and $ekorIdLama<=8){
					$ekorIdBaru = '000'.($ekorIdLama+1);
				}
				elseif($ekorIdLama>=9 and $ekorIdLama<=98){
					$ekorIdBaru = '00'.($ekorIdLama+1);
				}
				elseif($ekorIdLama>=99 and $ekorIdLama<=998){
					$ekorIdBaru = '0'.($ekorIdLama+1);
				}
				elseif($ekorIdLama>=999 and $ekorIdLama<=9998){
					$ekorIdBaru = $ekorIdLama+1;
				}
				else{
					return false;
				}
				
				$idOrderBaru = str_replace("-","",$tglSekarang).$ekorIdBaru;
			}
			
			$this->idOrder = $idOrderBaru;
		}
		public function resetIdOrder($idOrder){
			$this->idOrder = $idOrder;
		}
		public function getIdOrder(){
			return $this->idOrder;			
		}
		public function setTglOrder($tglOrder){
			$this->tglOrder = $tglOrder;
		}		
		public function getTglOrder(){
			return $this->tglOrder;			
		}
		public function setSubtotal($subtotal){
			$this->subtotal = $subtotal;
		}		
		public function getSubtotal(){
			return $this->subtotal;			
		}
		public function setBuktiTrf($buktiTrf){
			$this->buktiTrf = $buktiTrf;
		}		
		public function getBuktiTrf(){
			return $this->buktiTrf;			
		}
		public function setStatus($status){
			$this->status = $status;
		}		
		public function getStatus(){
			return $this->status;			
		}
		public function setIdAdmin($idAdmin){
			$this->idAdmin = $idAdmin;
		}		
		public function getIdAdmin(){
			return $this->idAdmin;			
		}
		public function setUsername($username){
			$this->username = $username;
		}
		public function getUsername(){
			return $this->username;
		}
		
		function orderBaru(){
			$sql = "INSERT INTO tb_order (idOrder, tglOrder, subtotal, status, username) VALUES ('".$this->getIdOrder()."','".$this->getTglOrder()."','".$this->getSubtotal()."','".$this->getStatus()."','".$this->getUsername()."')";
			$query = mysql_query($sql) or die ("Error Buat Order Baru: ".mysql_error());
			return $query;
		}
		function cekOrderbyStatUser(){
			$sql = "SELECT *FROM tb_order WHERE username='".$this->getUsername()."' AND status='".$this->getStatus()."'";
			$query = mysql_query($sql) or die ("Error Cek Order: ".mysql_error());
			
			if(mysql_num_rows($query)==0){
				return 0;
			}
			else{
				$row = mysql_fetch_array($query);
				$this->idOrder = $row['idOrder'];
				
				return 1;
			}
		}
		function cekSemuaOrderanByUser(){
			$sql = "SELECT *FROM tb_order WHERE username='".$this->getUsername()."' ORDER BY tglOrder DESC";
			$query = mysql_query($sql) or die ("Error: ".mysql_error());
			
			if(mysql_num_rows($query)==0){
				return FALSE;
			}
			else{
				return $sql;
			}
		}
		function cekOrderByIdOrder(){
			$sql = "SELECT *FROM tb_order WHERE idOrder='".$this->idOrder."'";
			$query = mysql_query($sql) or die ("Error Cek Order: ".mysql_error());
			
			if(mysql_num_rows($query)==0){
				return 0;
			}
			else{
				$r = mysql_fetch_array($query);
				$this->setStatus($r['status']);
				$this->setBuktiTrf($r['buktiTrf']);
				$this->setIdAdmin($r['idAdmin']);
				$this->setTglOrder($r['tglOrder']);
				$this->setSubtotal($r['subtotal']);
				
				return $query;
			}
		}
		function updateSubtotal(){
			$sql = "UPDATE tb_order SET subtotal=subtotal+".$this->getSubtotal()." WHERE idOrder='".$this->getIdOrder()."'";
			$query = mysql_query($sql) or die("Error Update subtotal");
			
			if($query)
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}
		function updateSubBaru(){
			$sql = "UPDATE tb_order SET subtotal=".$this->getSubtotal()." WHERE idOrder='".$this->getIdOrder()."'";
			$query = mysql_query($sql) or die("Error Update subtotal");
			
			if($query)
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}
		function hapusOrderByIdOrder(){
			$sql = "DELETE FROM tb_order WHERE idOrder='".$this->getIdOrder()."'";
			$query = mysql_query($sql) or die ("Error Cek Order: ".mysql_error());
			
			if($query){
				return 1;
			}
			else{
				return 0;
			}
		}
		function updateStatOrder(){
			$sql = "UPDATE tb_order SET status='".$this->getStatus()."' WHERE idOrder='".$this->getIdOrder()."'";
			$query = mysql_query($sql) or die("Error Update Status Order");
			
			if($query)
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}
		function updateGbrBuktiTrf(){
			$sql = "UPDATE tb_order SET buktiTrf='".$this->getBuktiTrf()."' WHERE idOrder='".$this->getIdOrder()."'";
			$query = mysql_query($sql) or die("Error Update Bukti Transfer Order: ".mysql_error());
			
			if($query)
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}
		
		
		
		
	}
	class waktu{
		private $waktu;
		private $waktuSekarang;
		
		function setWaktu($waktu){
			$this->waktu = $waktu;
		}
		function getWaktu(){
			return $this->waktu;
		}
		function setWaktuSekarang(){
			$this->waktuSekarang = gmdate("Y-m-d H:i:s", time()+60*60*7);
		}
		function getWaktuSekarang(){
			return $this->waktuSekarang;
		}
		
		/*=========================== Fungsi Perubah Format Waktu ===================================*/
		
		function format_tgl1($parameter){
	//^Membuat fungsi format_tgl yang akan memeproses nilai yang ditaruh di parameter (Diharapkan nilainya adalah tgl yyyy-mm-dd)
	
	$tanggal = substr($parameter,8,2);
	$bln_angka = substr($parameter,5,2);
	$tahun = substr($parameter,0,4);
	
	switch ($bln_angka){
		case 1:
			$bulan = "Januari";
			break;
			
		case 2:
			$bulan = "Februari";
			break;
		
		case 3:
			$bulan = "Maret";
			break;
		
		case 4:
			$bulan = "April";
			break;
			
		case 5:
			$bulan = "Mei";
			break;
			
		case 6:
			$bulan = "Juni";
			break;
		
		case 7:
			$bulan = "Juli";
			break;
		
		case 8:
			$bulan = "Agustus";
			break;
			
		case 9:
			$bulan = "September";
			break;
			
		case 10:
			$bulan = "Oktober";
			break;
		
		case 11:
			$bulan = "November";
			break;
		
		case 12:
			$bulan = "Desember";
			break;
	}
	
	return $tanggal.' '.$bulan.' '.$tahun;//Return berfungsi untuk mengubah nilai parameter menjadi nilai yang sudah diproses
}

//fungsi format tgl 2: display tanggal 12/08/2015, dari format tanggal 2015-08-12
function format_tgl2($parameter){
	$tanggal = substr($parameter,8,2);
	$bulan = substr($parameter,5,2);
	$tahun = substr($parameter,0,4);
	
	return $tanggal.'/'.$bulan.'/'.$tahun;
}

//fungsi format tgl dengan nama bulan disingkat
function format_tgl3($parameter){
	//^Membuat fungsi format_tgl yang akan memeproses nilai yang ditaruh di parameter (Diharapkan nilainya adalah tgl yyyy-mm-dd)
	
	$tanggal = substr($parameter,8,2);
	$bln_angka = substr($parameter,5,2);
	$tahun = substr($parameter,0,4);
	
	switch ($bln_angka){
		case 1:
			$bulan = "Jan";
			break;
			
		case 2:
			$bulan = "Feb";
			break;
		
		case 3:
			$bulan = "Mar";
			break;
		
		case 4:
			$bulan = "Apr";
			break;
			
		case 5:
			$bulan = "Mei";
			break;
			
		case 6:
			$bulan = "Jun";
			break;
		
		case 7:
			$bulan = "Jul";
			break;
		
		case 8:
			$bulan = "Agu";
			break;
			
		case 9:
			$bulan = "Sep";
			break;
			
		case 10:
			$bulan = "Okt";
			break;
		
		case 11:
			$bulan = "Nov";
			break;
		
		case 12:
			$bulan = "Des";
			break;
	}
	
	return $tanggal.' '.$bulan.' '.$tahun;//Return berfungsi untuk mengubah nilai parameter menjadi nilai yang sudah diproses
}

function format_tgl4($parameter){
	//^Membuat fungsi format_tgl yang akan memeproses nilai yang ditaruh di parameter (Diharapkan nilainya adalah tgl yyyy-mm-dd)
	
	$tanggal = substr($parameter,8,2);
	$bln_angka = substr($parameter,5,2);
	$tahun = substr($parameter,0,4);
	
	switch ($bln_angka){
		case 1:
			$bulan = "Januari";
			break;
			
		case 2:
			$bulan = "Februari";
			break;
		
		case 3:
			$bulan = "Maret";
			break;
		
		case 4:
			$bulan = "April";
			break;
			
		case 5:
			$bulan = "Mei";
			break;
			
		case 6:
			$bulan = "Juni";
			break;
		
		case 7:
			$bulan = "Juli";
			break;
		
		case 8:
			$bulan = "Agustus";
			break;
			
		case 9:
			$bulan = "September";
			break;
			
		case 10:
			$bulan = "Oktober";
			break;
		
		case 11:
			$bulan = "November";
			break;
		
		case 12:
			$bulan = "Desember";
			break;
	}
	
	$dayList = array(
	'Sun' => 'Minggu',
	'Mon' => 'Senin',
	'Tue' => 'Selasa',
	'Wed' => 'Rabu',
	'Thu' => 'Kamis',
	'Fri' => 'Jumat',
	'Sat' => 'Sabtu'
	);
	
	$hari = date('D', strtotime($parameter));
	
	return $dayList[$hari].', '.$tanggal.' '.$bulan.' '.$tahun;//Return berfungsi untuk mengubah nilai parameter menjadi nilai yang sudah diproses
}

function format_jam1($parameter){
	$jam = substr($parameter, 11, 5);
	return $jam;
}
		
	}
	class uploadFile{
		private $maxSize, $folder, $oldName, $newName, $type, $msgError, $fileSize, $tmpName;
		
		public function setMaxSize($maxSize){
			$this->maxSize = $maxSize;
		}
		public function getMaxSize(){
			return $this->maxSize;
		}
		public function setFolder($folder){
			$this->folder = $folder;
		}
		public function getFolder(){
			return $this->folder;
		}
		public function setOldName($oldName){
			$this->oldName = $oldName;
		}
		public function getOldName(){
			return $this->oldName;
		}
		public function setNewName($newName){
			$this->newName = $newName;
		}
		public function getNewName(){
			return $this->newName;
		}
		public function setType($type){
			$this->type = $type;
		}
		public function getType(){
			return $this->type;
		}
		public function setMsgError($msgError){
			$this->msgError = $msgError;
		}
		public function getMsgError(){
			return $this->msgError;
		}
		public function setFileSize($fileSize){
			$this->fileSize = $fileSize;
		}
		public function getFileSize(){
			return $this->fileSize;
		}
		public function setTmpName($tmpName){
			$this->tmpName = $tmpName;
		}
		public function getTmpName(){
			return $this->tmpName;
		}
		
		
		
		function JpegPngGif(){
			//Memeriksa berkas file yang diunggah (Harus JPEG/PNG/GIF)
			if(!($this->getType() == 'image/jpeg' or $this->getType() == 'image/png' or $this->getType() == 'image/gif')){		
				$this->setMsgError("Tipe file yang harus diunggah adalah jpeg/png/gif.");
				return 0;
			}
			else{
				//Memeriksa ukuran file yang diizinkan
				if($this->getFileSize() >= $this->getMaxSize()){
				$this->setMsgError("Ukuran File Maksimum adalah ".($this->getMaxSize()/1048576)." MB.");				
				return 0;
				}
				else{
					//menentukan extension gambar sesuai dengan format yang upload pengguna
					if($this->getType() == 'image/jpeg'){
						$format = 'jpg';
					}
					elseif($this->getType() == 'image/png'){
						$format = 'png';
					}
					else{
						$format = 'gif';
					}
					
					//inisisalisasi parameter unggah gambar
					$folderTujuan = $this->getFolder(); //menetukan folder tempat menyimpan gambar di server (htdoc di local host)
					$nmFileBaru = $this->getNewName().'.'.$format; //menetukan nama file dan ekstensinya
					$tmp_file = $this->getTmpName(); //menetukan tmp_name file yang diupload
					
					if(move_uploaded_file($tmp_file, $folderTujuan.'/'.$nmFileBaru) == TRUE){//Jika upload gambar berhasil
						$this->setNewName($nmFileBaru);
						return 1;
					}
					else{
						$this->setMsgError("Tidak Diketahui.");
						return 0;
					}
					
				}
			}
		}
	}
	class uang{
		private $nominal;
		public function setNominal($nominal){
			$this->nominal = $nominal;
		}
		public function getNominal(){
			return $this->nominal;
		}
		
		function formatUang(){
			$awal = $this->getNominal();
			
			$pjg = strlen($awal);
			
			$z = 1;
			for($i=$pjg; $i>=1; $i--){
				
			}
		}
	}
	
?>