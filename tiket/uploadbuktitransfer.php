<?php
if($_SERVER['REQUEST_METHOD'] == 'POST'){
	include_once ('include/class.php');
	
	$nmGbr = $_FILES['buktiTrf']['name'];
	$szGbr = $_FILES['buktiTrf']['size'];
	$tpGbr = $_FILES['buktiTrf']['type'];
	$tpnmGbr = $_FILES['buktiTrf']['tmp_name'];
	$urlTujuan = 'buktiTrf';
	$maxSize = 2097152; //2097152 B = 2MB
	
	
	
	$upload = new uploadFile;
	$upload->setFileSize($szGbr);
	$upload->setFolder($urlTujuan);
	$upload->setTmpName($tpnmGbr);
	$upload->setNewName($_POST['idOrder']);
	$upload->setType($tpGbr);
	$upload->setMaxSize($maxSize);
	
	if($upload->JpegPngGif()==1){//Jika berhasil upload bukti transfer
		$order = new COrder;
		$order->setBuktiTrf($upload->getNewName());
		$order->resetIdOrder($_POST['idOrder']);
		
		if($order->updateGbrBuktiTrf()==1){//Jika field buktiTrf di database sudah disi
			//lakukan perubahan status menjadi
			$order->setStatus('Pembayaran Diterima, Menunggu Verifikasi');
			
			if($order->updateStatOrder()){
				$pesan = 'Berhasil Mengunggah Bukti Transfer. Mohon Menunggu Proses Verifikasi ;)';
				$halamanBerikutnya = '?page=orderan';
			}
		}
		
	}
	else{
		$pesan = $upload->getMsgError();
		$halamanBerikutnya = '?page=uploadbuktitransfer&no_order='.$_POST['idOrder'];
	}
	?>
    <script>
		alert('<?php echo $pesan;?>');
		window.location='<?php echo $halamanBerikutnya;?>';
	</script>
    <?php
		
}
else
{
	$idOrder = $_GET['no_order'];
?>
<h2 class="text-center">
	Unggah Bukti Transfer
</h2>
<br>
<center>Nomor Order: <?php echo $idOrder; ?></center>
<div class="container">
<hr>
<center>
Klik tombol berikut untuk mengunggah bukti transfer (format yang diizinkan jpg,jpeg,png atau gif ukuran kurang dari 2 MB).
<br>
<br>
<form method="post" enctype="multipart/form-data">
	<input type="hidden" name="idOrder" value="<?php echo $idOrder;?>"/>
	<input type="file" name="buktiTrf" required style="border-width:1px;"/>
    <br>
    <input type="submit" name="OK" value="UPLOAD" />
</form>
</center>                
</div>
<?php 
}
?>