<?php
	session_start();
	include_once('include/class.php');
	$db = new database;
	$db->connectmysql();	
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Pembelian Tiket Konser</title>

<!-- Bootstrap -->
<link rel="stylesheet" href="css/bootstrap.css">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<nav style="background-color:#00A2DD;">
  <div class="container"> 
    
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      <a class="navbar-brand" href="../tiket" style="color:white;">Beranda</a> </div>
    
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse">
      <!-- <ul class="nav navbar-nav">
        <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a> </li>
        <li><a href="#">Link</a> </li>
        <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Dropdown <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Action</a> </li>
            <li><a href="#">Another action</a> </li>
            <li><a href="#">Something else here</a> </li>
            <li class="divider"></li>
            <li><a href="#">Separated link</a> </li>
            <li class="divider"></li>
            <li><a href="#">One more separated link</a> </li>
          </ul>
        </li>
      </ul> -->
      <form class="navbar-form navbar-right" method="get" role="search">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Cari" name="cari">
        </div>
        <button type="submit" class="btn btn-default">Cari</button>
      </form>
      <ul class="nav navbar-nav navbar-right hidden-sm">
        <li class="dropdown">
<?php
	
	if(isset($_SESSION['loginMember']) && $_SESSION['loginMember'] == TRUE){
		$user = new member();
		$user->setUsername($_SESSION['usernameMember']);
		$user->setPassword($_SESSION['passwordMember']);
		
		$list = $user->cekUsernameDanPassword();
			
		?>
		<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" style="background-color:white;">Hi, <?php echo $list['nama']; ?>! <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="?page=lihatprofil">Lihat Profil</a> </li>
			<li><a href="?page=orderan">Orderan Saya</a> </li>
            <li class="divider"></li>
			<li><a href="?page=ubahprofil">Ubah Profil</a> </li>
            <li><a href="memberlogout.php" onClick="return confirm('Anda Yakin Ingin Keluar?');">Keluar</a> </li>
          </ul>
		 <?php ; 
	}
	else{
		echo '
		<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" style="background-color: #ffffff;">Hi, Pengunjung!<span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="?page=memberlogin">Masuk</a> </li>
            <li class="divider"></li>
            <li><a href="?page=daftarmember">Daftar</a> </li>
          </ul>
		 ';
	}
?>
        </li>
      </ul>
    </div>
    <!-- /.navbar-collapse --> 
  </div>
  <!-- /.container-fluid --> 
</nav>
<hr>
<div class="main" style="min-height:350px">

	<?php
		//Halaman akan ditampilkan disini
		include "isi.php"; 
	?>
</div>
<hr>

  <div class="container well"  style="background-color:#00A2DD;">
    <div class="row">
      <!--<div class="col-xs-6 col-sm-6 col-md-6 col-lg-7">
      <strong>Lakukan Pembayaran Melalui:</strong>
        <div class="row">
          <div class="col-sm-4 col-md-4 col-lg-4 col-xs-6">
            <div>
             	Mandiri
            </div>
          </div>
          <div class="col-sm-4 col-md-4 col-lg-4  col-xs-6">
            <div>
             BCA
            </div>
          </div>
          <div class="col-sm-4 col-md-4 col-lg-4 col-xs-6">
            BRI
          </div>
        </div>
      </div>-->
      <font color="#F5F5F5">
      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-5"> 
        <address>
        <strong>Kelompok 8 Web Programming 3</strong><br>
        Kelas 11.7C.01
        (Nusa Mandiri Jakarta)<br>
        <abbr title="Phone"></abbr> (021) 3100413<br>
        Jalan Kramat Raya No 18, Jakarta Pusat
      </address>
        <!-- <address>
        <strong>STMIK Nusa Mandiri</strong>
        <br>
        <a href="mailto:#">stmik.jakarta@nusamandiri.ac.id</a>
        </address> -->
        </div>
      </font>
    </div>
  </div>

<footer class="text-center">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <p>Copyright © Kelompok 3 Nusa Mandiri. All rights reserved.</p>
      </div>
    </div>
  </div>
</footer>
<script src="js/jquery-1.11.2.min.js"></script> 
<script src="js/bootstrap.min.js"></script>
</body>
</html>