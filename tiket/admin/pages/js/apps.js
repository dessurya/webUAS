$(document).ready(function(){
	$.ajax({
		url: "http://localhost/cart/data.php",
		method: "GET",
		success: function(data) {
			console.log(data);
			var bulan = [];
			var jumlah = [];

			for(var i in data) {
				bulan.push("Bulan " + data[i].bulan);
				jumlah.push(data[i].jumlah);
			}

			var chartdata = {
				labels: bulan,
				datasets : [
					{
						label: 'Transaksi ',
						backgroundColor: 'rgba(200, 200, 200, 0.75)',
						borderColor: 'rgba(200, 200, 200, 0.75)',
						hoverBackgroundColor: 'rgba(200, 200, 200, 1)',
						hoverBorderColor: 'rgba(200, 200, 200, 1)',
						data: jumlah
					}
				]
			};

			var ctx = $("#mycanvas");

			var barGraph = new Chart(ctx, {
				type: 'bar',
				data: chartdata
			});
		},
		error: function(data) {
			console.log(data);
		}
	});
});