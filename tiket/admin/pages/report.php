<?php

error_reporting(E_ERROR | E_PARSE);


?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | ChartJS</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="http://localhost/tiket/admin/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="http://localhost/tiket/admin/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="http://localhost/tiket/admin/dist/css/skins/_all-skins.min.css">
  <script src="http://localhost/tiket/admin/plugins/w3data.js"></script>


  <!--
  <script type="text/javascript" src="http://localhost/tiket/admin/pages/js/apps.js"></script>

  <script src="http://localhost/tiket/admin/plugins/jQuery/jquery-2.2.3.min.js"></script>

  <script src="http://localhost/tiket/admin/bootstrap/js/bootstrap.min.js"></script>

  <script src="http://localhost/tiket/admin/plugins/chartjs/Chart.min.js"></script>

  <script src=".http://localhost/tiket/admin/plugins/fastclick/fastclick.js"></script> -->


</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  
  <!-- Left side column. contains the logo and sidebar -->
  <div w3-include-html="http://localhost/tiket/admin/header.html"></div>  
  <div w3-include-html="http://localhost/tiket/admin/menu.html"></div>
  
  <script>
    w3IncludeHTML();
  </script>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Penjualan
        <small></small>
      </h1>
      
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- AREA CHART -->
          <div class="box box-primary">
            <div class="box-header with-border">
              

              
            </div>
            <div class="box-body">
              
              <div class="chart">
            <canvas id="mycanvas"></canvas>
            <script type="text/javascript" src="http://localhost/tiket/admin/plugins/jQuery/jquery-2.2.3.min.js"></script>
              <script type="text/javascript" src="http://localhost/tiket/admin/pages/js/Chart.js"></script>
              <script type="text/javascript" src="http://localhost/tiket/admin/pages/js/apps.js"></script>
          </div>
              
              
            </div>
            <!-- /.box-body -->
          </div>

          


        </div>
        <!-- /.col (LEFT) -->
        
        <!-- /.col (RIGHT) -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->

    

    <!-- javascript -->
    
    


    </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- page script -->

</body>
</html>
