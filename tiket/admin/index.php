<?php 

error_reporting(E_ERROR | E_PARSE);


include( $_SERVER['DOCUMENT_ROOT'] . '/tiket/include/controller_pesanan.php' ); 
$db = new admin();
include "pages/header.php";
?>
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        
      </h1>
      
    </section>

    <!-- Main content -->
    <section class="content">
    Berikut ini statistik pesanan terbaru.
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <div class="col-md-12">
          <div class="box">
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Pesanan Terkini</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin">
                  
                  <thead>
                  <tr>
                    
                    <th>Kode </th>
                    <th>Nama Pemesan</th>
                    <th>Tanggal Pemesan</th>
                    <th>Status</th>
                    <th>Total (IDR)</th>
                    
                  </tr>
                  </thead>
                  <tbody>
                  <?php
                  $no = 1;
                    foreach($db->tampilPesananTerkini() as $x){
                  ?>
                  <tr>
                  
                    <td><?php echo $x['idOrder']; ?></td>
                    <td><?php echo $x['username']; ?></td>
                    <td><?php echo $x['tglOrder']; ?></td>
                    <td><span class="label label-success"><?php echo  $x['status']?></span></td>
                    <td align='right'><?php echo  number_format($x['subtotal']);?></td>
                    
                  </tr>
                  <?php 
                    }
                  ?>
                  
                  </tbody>
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
              
              <a href="http://localhost/tiket/admin/pages/pesanan.php" class="btn btn-sm btn-default btn-flat pull-right">Tampilkan Semua Pesanan</a>
            </div>
            <!-- /.box-footer -->
          </div>
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->

        
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

<?php
include "pages/footer.php"
?>