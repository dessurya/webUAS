<?php
include_once ('include/class.php');
	if(isset($_SESSION['loginMember']) && $_SESSION['loginMember'] == TRUE){
		$tiket = new tiket;
		$waktu = new waktu;
		$member = new member;
		$member->setUsername($_SESSION['usernameMember']);
		$waktu->setWaktuSekarang();
			
		if(isset($_GET['evenID']) or isset($_GET['tambahtiket'])) // Jika sebelumnya pelanggan mengklik tombol order di halaman list tiket atau halaman rincian tiket
		{
			$tiket->setIdTiket($_GET['evenID']);
			$tiket->cariRincianTiket();
			
			if($tiket->cariRincianTiket() == '0') // Jika kode tiket yang dipilih tidak ada di tabel tiket, maka:
			{
				
				?>
                	<script language="javascript">
						alert('Terjadi Kesalahan');
					</script>
                <?php
			}
			else
			{
				$stokAwalTiket = $tiket->getStok();
				
				$order = new COrder;
				$dtOrder = new dtOrder;
				
				$order->setStatus('Menunggu Pembayaran');
				$order->setUsername($_SESSION['usernameMember']);
				$order->setSubtotal($tiket->getHarga());
				$dtOrder->setIdTiket($tiket->getIdTiket());
				$dtOrder->setUsername($_SESSION['usernameMember']);
				
				
				
				if($order->cekOrderbyStatUser()==0)//Cek apakah di orderan ada barang yang sudah ditambahkan dengan status 'Menunggu Pembayaran'
				{				
					$order->setTglOrder($waktu->getWaktuSekarang());
					$order->setIdOrder();	
										
					if($order->orderBaru())//Jika entry order baru berhasil ditambah
					{
						$dtOrder->setIdOrder($order->getIdOrder());
						$dtOrder->setJmlTiket(1);//1 Klik untuk 1 Tiket
						$dtOrder->getIdDiskon(0);//temporary
						
						if($dtOrder->tambahDtOrder() == 1)//Jika berhasil menambahkan 1 tiket ke orderan
						{
							$tiket->setJumlahAkhir($stokAwalTiket-1); //stok tiket dikurangi 1;
							
							if($tiket->updateStokById()){
								?>
                                	<script>
										alert("1 Tiket Konser <?php echo $tiket->getNmEven();?>");
									</script>
                                <?php
							}
						}
					}
				}
				else
				{
					$dtOrder->setStatus($order->getStatus());
					$dtOrder->setIdOrder($order->getIdOrder());//Mengambil idOrder yang sudah ada dari tabel order;		
					$stokAkhirTiket = $stokAwalTiket-1;
					$tiket->setJumlahAkhir($stokAkhirTiket);					
					$dtOrder->setJmlTiket(1);//Jumlah tiket akan ditambahkan 1
					
					if($dtOrder->cekDtOrderByIdTiket() == 1)//Jika ada tiket yang sama ditambahkan dengan idOrder sama dan idTiket sama
					{	
					$dtOrder->setIdOrder($order->getIdOrder());
					//echo $dtOrder->getIdOrder()."-".$dtOrder->getJmlTiket()."-".$dtOrder->getIdTiket();	
					$dtOrder->setJmlTiket(1);//Jumlah tiket akan ditambahkan 1		
						if($dtOrder->updateJmlOrderan() == 1)//Jika jumlah tiket berhasil ditambahkan (+1), maka:
						{						
							if($order->updateSubtotal()== 1)//Jika subtotal orderan di tabel tb_order bertambah
							{
								if($tiket->updateStokById()==1)//Jika stok tiket di tabel tiket terkurangi
								{
									?>
										<script>
											alert('Tiket <?php echo $tiket->getNmEven();?> sudah ada di orderan yang belum dibayar, tiket ditambahakan 1 lagi.');
										</script>
									<?php
								}
							}
						}
						
					}
					else//Jika tiket yang dipilih berbeda dari sebelum-sebelumnya pada orderan dengan status 'Menunggu Pembayaran'
					{
						$dtOrder->setIdDiskon(0);//Temporary
						
						if($dtOrder->tambahDtOrder()==1)//Jika berhasil memasukan 1 Tiket ke detail order
						{
							if($order->updateSubtotal()==1){//Jika subtotal berhasil ditambah
								if($tiket->updateStokById()){
									?>
										<script>
											alert('Tiket <?php echo $tiket->getNmEven();?> Berhasil Ditambahkan.');
										</script>
									<?php
								}
							}
						}
						
					}
				}
				
				
			}
			?>
            	<script>
					window.location='?page=orderan';
				</script>
            <?php
		}
		
		elseif((isset($_GET['page'])) and ($_GET['page']=='orderan'))//Jika custumer mengklik menu orderan
		{
			$order = new COrder;
			$order->setUsername($member->getUsername());
			
			if($order->cekSemuaOrderanByUser()==FALSE)//Jika tidak ada histori orderan sebelumnya
			{
				$kalimat = "<center><br>Sejauh ini tidak ada orderan di akun anda.<br><a href='/tiket'>Kembali ke Beranda</a></br></center>";
			}
			else{
				$query = mysql_query($order->cekSemuaOrderanByUser());
				$kalimat = '';
				while($row = mysql_fetch_array($query))//Jika ada histori orderan
				{
					$idOrder = $row['idOrder'];
					$tglOrder = $row['tglOrder'];
					$subtotal = $row['subtotal'];
					$status = $row['status'];
					
					{
						$unggahpembayaran = '<a href="?page=uploadbuktitransfer&no_order='.$idOrder.'">Unggah Bukti Pembayaran</a>';
						$batalkanorder = '<a href="?page=batalorder&no_order='.$idOrder.'" onClick="return confirm(\'Anda Yakin Orderan Terpilih Akan Dibatalkan\')">Batalkan Order</a>';
						
						if($subtotal == 0){
							$lihatdetail = '<s>Lihat Detail</s>';
							$unggahpembayaran = '<s>Unggah Pembayaran</s>';
							$batalkanorder =  '<s>Batalkan Order</s>';
							$printTicket = '<s>Cetak Tiket</s>';
						}
						else{
							$lihatdetail = '<a href="?page=detailorder&no_order='.$idOrder.'">Lihat Detail</a>';
							$printTicket = '<a href="printTiket.php">Cetak Tiket</a>';
							$_SESSION['id_Order'] = $idOrder;
						}
						
						
						
						if($status == 'Menunggu Pembayaran'){
							$opsi = $lihatdetail.' | '.$unggahpembayaran.' | '.$batalkanorder; 
						}
						elseif
						($status == 'Pembayaran Diterima, Menunggu Verifikasi'
						or $status == 'Pembayaran Terverifikasi'
						or $status == 'Pembayaran Tidak Teverifikasi'
						){
							$opsi = $lihatdetail.' | '.$batalkanorder; 
						}
						elseif
						($status == 'Transaksi Dibatalkan Pembeli'
						or $status == 'Transaksi Dibatalkan Sistem'
						or $status == 'Transaksi Selesai'
						){
							$opsi = $lihatdetail.' | '.$printTicket;
						}
						
						
					}
					
					{$kalimat1 = '
						<div id="list_order">
							<table width="50%">
								<tr>
									<td width="30%">
										No. Order
									</td>
									<td>
										:
									</td>
									<td>
										'.$idOrder.'
									</td>
								</tr>
								<tr>
									<td>
										Tanggal Order
									</td>
									<td>
										:
									</td>
									<td>
										'.$waktu->format_tgl1($tglOrder).'
									</td>
								</tr>
								<tr>
									<td>
										Subtotal
									</td>
									<td>
										:
									</td>
									<td>Rp. 
										'.number_format($subtotal).'
									</td>
								</tr>
								<tr>
									<td>
										Status
									</td>
									<td>
										:
									</td>
									<td>
										'.$status.'
									</td>
								</tr>
							</table>
							<br>
							<center>
								'.
									$opsi
								.'
							</center>
						</div>
					';}
					$kalimat = $kalimat.$kalimat1;
				}
			}
			
			?>
            <h2 class="text-center">
				Orderan Anda
            </h2>
            <div class="container">
                <hr>
                <?php
					echo $kalimat;
				?>
            </div>
            <?php
		}
		
	}
	else //Jika pengunjung belum login, maka akan diarahkan ke halaman login terlebih dahulu
	{
		?>
        <script language="javascript">
			alert('Anda harus login terlebih dahulu :)');
			window.location = '?page=memberlogin&nextpage=tambahtiket&evenID=<?php echo $_GET['evenID'];?>';
		</script>
        <?php
	}
?>