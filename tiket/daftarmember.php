<?php
	include_once ("include/class.php");
	if(isset($_SESSION['loginMember']) && $_SESSION['loginMember'] == TRUE){
		?>
        	<script language="javascript">
				document.location='../tiket'; //Jika sebelumnya user sudah berhasil login maka user akan dialihkan ke halaman beranda
			</script> 
        <?php
	}
	else{
		if($_SERVER['REQUEST_METHOD'] == 'POST'){
			$pesanError = '<br><span style="color:red;">Terjadi Kesalahan, diakibatkan:<br>';
			$error = 0;
			
			$username = $_POST['username'];
			$password = $_POST['password'];
			$rePassword = $_POST['Repassword'];
			$email = $_POST['email'];
			$nama = $_POST['nama'];
			
			$user = new member();
			$user->setUsername($username);
			$user->setPassword($password);
			$user->setNama($nama);
			$user->setEmail($email);
			
			if($password != $rePassword){
				$pesanError = $pesanError."&raquo; Password dan pengulangannya tidak cocok<br>";
				$error = $error+1;
				$rePassword = "";
				$password = "";
			}
			
			if($user->cekUsername() != FALSE){
				$pesanError = $pesanError."&raquo; Username sudah digunakan<br>";
				$error = $error+1;
				$username = "";
			}
			
			if($user->cekEmail() != FALSE){
				$pesanError = $pesanError."&raquo; Email sudah digunakan<br>";
				$error = $error+1;
				$email = "";
			}			
			
			$pesanError = $pesanError."</span>";
			if($error == 0){
				if($user->memberBaru() == TRUE){
					?>
                		<script language="javascript">
							alert('Anda Berhasil Mendaftar sebagai Member Baru. Silahkan Login!');
							document.location="../tiket/?page=memberlogin";
						</script>
                	<?php
				}
			}
		}
	}
?>
<center>
	<h1>Daftar Sebagai Member</h1><br>
<form method="post">
	<table style="font-size:14px" align="center" width="42%">
    	<tr>
        	<td width="28%">
            	<h4>Usename</h4>
            </td>
            <td width="5%">
            	<h4>:</h4>
            </td>
            <td width="57%">
            	<input type="text" name="username" id="username" maxlength="15" class="form-control" style="width:300px;" placeholder="5-15 Karakter" required value="<?php if(isset($username)){echo $username;} ?>">
            </td>
            <td width="10%">
            	<span id="userCapt">
                </span>
            </td>	
        </tr>
        <tr>
        	<td>
            	<h4>Password</h4>
            </td>
            <td>
            	<h4>:</h4>
            </td>
            <td>
            	<input type="password" name="password" id="password" maxlength="15" class="form-control" style="width:300px;" placeholder="4-15 Karakter (kapital, non kapital & angka)" required pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{4,}">
            </td>
            <td>
            	<span id="passCapt">
                </span>
            </td>
        </tr>
        <tr>
        	<td>
            	<h4>Ulangi Password</h4>
            </td>
            <td>
            	<h4>:</h4>
            </td>
            <td>
            	<input type="password" name="Repassword" id="Repassword" maxlength="15" class="form-control" style="width:300px;" placeholder="4-15 Karakter (kapital, non kapital & angka)" required pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{4,}">
            </td>
            <td>
            	<span id="passCapt">
                </span>
            </td>
        </tr>
        <tr>
        	<td>
            	<h4>Email</h4>
            </td>
            <td>
            	<h4>:</h4>
            </td>
            <td>
            	<input type="email" name="email" id="email" class="form-control" style="width:300px;" placeholder="" required pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" value="<?php if(isset($email)){echo $email;} ?>">
            </td>
            <td>
            	<span id="emailCapt">
                </span>
            </td>
        </tr>
        <tr>
        	<td>
            	<h4>Nama Lengkap</h4>
            </td>
            <td>
            	<h4>:</h4>
            </td>
            <td>
            	<input type="text" name="nama" id="nama" maxlength="15" class="form-control" style="width:300px;" placeholder="1-15 Karakter" required pattern="[a-zA-Z\s]{1,30}" value="<?php if(isset($nama)){echo $nama;} ?>">
            </td>
            <td>
            	<span id="nama">
                </span>
            </td>
        </tr>
        <tr>
        	<td colspan="4">
            <br>
            	<input type="checkbox" required> Saya menyetujui <a href="#">persyaratan</a> yang ditetapkan.
            </td>
        </tr>
         <tr>
        	<td colspan="4" align="center">
            <br>
            	<input type="submit" name="Daftar" class="btn btn-default" value="Daftar">
            </td>
        </tr>
    </table>
</form>
<?php
	if(isset($pesanError)){
		echo $pesanError;
	}
?>
</center>
